(ns geolocation.core)

(def pi (. Math PI))
(def earth-radius-m 6371010.0) ; 6,371km

(defn deg->rad [deg]
  (Math/toRadians deg))

(defn rad->deg [rad]
  (Math/toDegrees rad))

(def maxlat   90.0)
(def minlat  -90.0)
(def maxlon  180.0)
(def minlon -180.0)

(def maxlatr (deg->rad maxlat))
(def minlatr (deg->rad minlat))
(def maxlonr (deg->rad maxlon))
(def minlonr (deg->rad minlon))


(defrecord coord
    [latdeg londeg 
     latrad lonrad])

(defn from-degs [latdeg londeg]
  (coord. latdeg 
          londeg
          (deg->rad latdeg)
          (deg->rad londeg)))

(defn from-rads [latrad lonrad]
  (coord. (rad->deg latrad)
          (rad->deg lonrad)
          latrad
          lonrad))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Bounding box calculations for a point on a sphere. Based on the
;; version here: 
;;
;;     http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
;;
;; The code presented there is assumed to be in the public
;; domain. Comments are reproduced where appropriate.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn calc-for-poles [minlatb maxlatb]
  (let [nminlatb (Math/max minlatb minlatr)
        nmaxlatb (Math/min maxlatb maxlatr)]

      {:min (from-rads nminlatb minlonr)
       :max (from-rads nmaxlatb maxlonr)}))

(defn calc-bounds [adist coords minlatb maxlatb]
  (let [deltalon (Math/asin (/ (Math/sin adist) (Math/cos (:latrad coords))))
        minlonb (- (:lonrad coords) deltalon)
        maxlonb (+ (:lonrad coords) deltalon)]

    {:min (from-rads minlatb 
                     (if (< minlonb minlonr)
                       (+ minlonb (* 2 pi))
                       minlonb))
     :max (from-rads maxlatb
                     (if (> maxlonb maxlonr)
                       (- maxlonb (* 2 pi))
                       maxlonb))}))


(defn calc-bounding-coords 
  "Computes the bounding coordinates of all points on the surface of a
   sphere that have a great circle distance to the point represented
   by the coordinate record that is less or equal to the distance
   argument. For more information about the formulae used in this
   method visit

       http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates

   'distance' is the distance from the point represented by this
   coordinate record. Must me measured in the same unit as the
   radius argument.  The optional parmeter 'radius' specifies the
   radius of the sphere, e.g. the average radius for a spherical
   approximation of the Earth is 6371.01 kilometers.

   Returns a map of two coordinate records specifying the minimum and
   maximum extents of the bounding box containing the circle of radius
   'distance'."

  ([coords distance] 
     (calc-bounding-coords coords distance earth-radius-m))

  ([coords distance radius]
     (when (or (< (:londeg coords) minlon) (> (:londeg coords) maxlon)
               (< (:latdeg coords) minlat) (> (:latdeg coords) maxlat)
               (<= distance 0) (<= radius 0))
       (throw (IllegalArgumentException. "Invalid parameter")))

     (let [adist (/ distance radius) ; Angular distance in radians on a great circle
           minlatbound (- (:latrad coords) adist)
           maxlatbound (+ (:latrad coords) adist)]
       
       (cond (and (> minlatbound minlatr) (< maxlatbound maxlatr))
             (calc-bounds adist coords minlatbound maxlatbound)
             :else
             (calc-for-poles minlatbound maxlatbound)))))


(defn calc-distance 
  "Computes the great circle distance between this two
   locations. radius is the radius of the sphere, e.g. the average
   radius for a spherical approximation of the figure of the Earth is
   approximately 6371.01 kilometers. Returns the distance, measured in
   the same unit as the radius argument (assumed to be meters)."

  ([from to]
     (calc-distance from to earth-radius-m))

  ([from to radius]
     (* radius
        (Math/acos (+ (* (Math/sin (:latrad from))
                         (Math/sin (:latrad to)))
                      (* (Math/cos (:latrad from))
                         (Math/cos (:latrad to))
                         (Math/cos (- (:lonrad from)
                                      (:lonrad to)))))))))
