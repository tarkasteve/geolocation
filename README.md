# geolocation

A Clojure library of geographic coordinate routines.

This library is based on algorithms described on this page:

    http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates

Current functions provide calculation of a bounding box around
lat/long coordinates and calculation of distance between two
coordinates.

## Usage

See unit tests for usage samples

## License

Copyright © 2014 Steve Smith

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
