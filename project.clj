(defproject net.haltcondition/geolocation "0.3.0-SNAPSHOT"
  :description "Library of geographic coordinate routines"
  :url "https://github.com/tarka/geolocation"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]])
