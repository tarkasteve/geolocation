(ns geolocation.core-test
  (:require [clojure.test :refer :all]
            [geolocation.core :refer :all]))

(deftest pi-test
  (is pi (. Math PI)))

(deftest deg200->rad
  (is (deg->rad 200) 3.49))

(deftest rad->deg200 
  (is (rad->deg 3.49) 200))

(deftest lat-lt-min-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs -91 1) 1))))
(deftest lat-gt-max-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs 91 1) 1))))
(deftest lon-lt-min-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs 1 -181) 1))))
(deftest lon-gt-max-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs 1 181) 1))))
(deftest dist-zero-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs 1 1) 0))))
(deftest dist-lt-zero-ex
  (is (thrown? IllegalArgumentException
               (calc-bounding-coords (from-degs 1 1) -1))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Tests generated from GeoLocation.java ;;;;;;;;;;;;;;;;;;;;

(defn close? [a b]
  (< (Math/abs (- a b)) 0.0000000001))

;; Equator:
;; Radians: -0.15696098420815538,-0.15696098420815538,0.15696098420815538,0.15696098420815538
;; Degrees: -8.993201943346866,-8.993201943346866,8.993201943346866,8.993201943346866
(deftest bound-equator
  (let [bounds (calc-bounding-coords (from-degs 0.0 0.0) 1000000)]
    (is (close? (:latrad (:min bounds)) -0.15696098420815538))
    (is (close? (:lonrad (:min bounds)) -0.15696098420815538))
    (is (close? (:latrad (:max bounds))  0.15696098420815538))
    (is (close? (:lonrad (:max bounds))  0.15696098420815538))
    (is (close? (:latdeg (:min bounds)) -8.993201943346866))
    (is (close? (:londeg (:min bounds)) -8.993201943346866))
    (is (close? (:latdeg (:max bounds))  8.993201943346866))
    (is (close? (:londeg (:max bounds))  8.993201943346866))))

;; North Pole:
;; Radians: 1.4138353425867412,-3.141592653589793,1.5707963267948966,3.141592653589793
;; Degrees: 81.00679805665314,-180.0,90.0,180.0
(deftest bound-npole
  (let [bounds (calc-bounding-coords (from-degs 90.0 0.0) 1000000)]
    (is (close? (:latrad (:min bounds))    1.4138353425867412))
    (is (close? (:lonrad (:min bounds))   -3.141592653589793))
    (is (close? (:latrad (:max bounds))    1.5707963267948966))
    (is (close? (:lonrad (:max bounds))    3.141592653589793))
    (is (close? (:latdeg (:min bounds))   81.00679805665314))
    (is (close? (:londeg (:min bounds)) -180.0))
    (is (close? (:latdeg (:max bounds))   90.0))
    (is (close? (:londeg (:max bounds))  180.0))))

;; South Pole:
;; Radians: -1.5707963267948966,-3.141592653589793,-1.4138353425867412,3.141592653589793
;; Degrees: -90.0,-180.0,-81.00679805665314,180.0
(deftest bound-spole
  (let [bounds (calc-bounding-coords (from-degs -90.0 0.0) 1000000)]
    (is (close? (:latrad (:min bounds))   -1.5707963267948966))
    (is (close? (:lonrad (:min bounds))   -3.141592653589793))
    (is (close? (:latrad (:max bounds))   -1.4138353425867412))
    (is (close? (:lonrad (:max bounds))    3.141592653589793))
    (is (close? (:latdeg (:min bounds))  -90.0))
    (is (close? (:londeg (:min bounds)) -180.0))
    (is (close? (:latdeg (:max bounds))  -81.00679805665314))
    (is (close? (:londeg (:max bounds))  180.0))))

;; Whitton Drive:
;; Radians: 0.7428796399730376,-0.259804618964929,1.0568016083893483,0.24849195325886242
;; Degrees: 42.56386805665314,-14.885708164694938,60.55027194334688,14.23754016469494
(deftest bound-whittondr
  (let [bounds (calc-bounding-coords (from-degs 51.55707 -0.324084) 1000000)]
    (is (close? (:latrad (:min bounds))   0.7428796399730376))
    (is (close? (:lonrad (:min bounds))  -0.259804618964929))
    (is (close? (:latrad (:max bounds))   1.0568016083893483))
    (is (close? (:lonrad (:max bounds))   0.24849195325886242))
    (is (close? (:latdeg (:min bounds))  42.56386805665314))
    (is (close? (:londeg (:min bounds)) -14.885708164694938))
    (is (close? (:latdeg (:max bounds))  60.55027194334688))
    (is (close? (:londeg (:max bounds))  14.23754016469494))))

;; The Castle:
;; Radians: 0.7431401827237752,-0.2601618909646153,1.057062151140086,0.2483052759438721
;; Degrees: 42.57879605665314,-14.906178342415162,60.56519994334687,14.226844342415161
(deftest bound-castle
  (let [bounds (calc-bounding-coords (from-degs 51.571998 -0.339667) 1000000)]
    (is (close? (:latrad (:min bounds))   0.7431401827237752))
    (is (close? (:lonrad (:min bounds))  -0.2601618909646153))
    (is (close? (:latrad (:max bounds))   1.057062151140086))
    (is (close? (:lonrad (:max bounds))   0.2483052759438721))
    (is (close? (:latdeg (:min bounds))  42.57879605665314))
    (is (close? (:londeg (:min bounds)) -14.906178342415162))
    (is (close? (:latdeg (:max bounds))  60.56519994334687))
    (is (close? (:londeg (:max bounds))  14.226844342415161))))

;; Distance: 1978.7769784208517
(deftest dist-whitton-castle
  (is (close? 1978.7769784208517
              (calc-distance (from-degs 51.55707 -0.324084)
                             (from-degs 51.571998 -0.339667)))))
